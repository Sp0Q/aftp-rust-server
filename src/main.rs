extern crate clap;
use clap::{App, Arg};

use rand::Rng;
use std::io::{prelude::*, BufReader, Read, Write};
use std::net::{TcpListener, TcpStream};
use std::str;
use std::thread;

mod auth;
mod list;
mod sync;

const AUTH: &'static str = "AUTH";
const PUB: &'static str = "PUB";
const LIST: &'static str = "LIST";
const GET: &'static str = "GET";
const PUT: &'static str = "PUT";
const DELETE: &'static str = "DELETE";

const CONTENTLENGTH: &'static str = "Content-Length";

fn main() {
    // argument declaration
    let matches = App::new("AFTP Server")
        .arg(
            Arg::with_name("FOLDER")
                .help("Sets the folder to use")
                .required(true)
                .index(1),
        )
        .get_matches();

    let listener = TcpListener::bind("0.0.0.0:8000").unwrap();
    let root_folder = String::from(
        matches
            .value_of("FOLDER")
            .expect("no folder argument found"),
    );

    // accept connections and process them, spawning a new thread for each one
    println!("Server listening on port 8000");

    // random number for DH
    let mut rng = rand::thread_rng();

    // TODO n should be random per session
    let n: u32 = rng.gen_range(2, 25);

    println!("n: {}", n);

    let pub_key = auth::PubKey { p: 23, g: 5 };

    println!("pub_key: p={},g={}", pub_key.p, pub_key.g);

    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                println!("New connection: {}", &stream.peer_addr().unwrap());
                let moved_root_folder = root_folder.clone();
                thread::spawn(move || {
                    // connection succeeded
                    handle_client(moved_root_folder.as_str(), &stream, pub_key, n)
                });
            }
            Err(e) => {
                // connection failed
                println!("Error: {}", e);
            }
        }
    }
    // close the socket server
    drop(listener);
}

fn handle_client(root_folder: &str, stream: &TcpStream, pub_key: auth::PubKey, n: u32) {
    let mut reader = BufReader::new(stream);

    loop {
        let mut req: Vec<u8> = Vec::new();
        let mut content_length: usize = 0;

        let iter = reader.by_ref().lines();
        for line in iter {
            // put all lines in buffer
            // send to function when empty line (only when content-length = 0)
            // if content-length > 0 => also send content to function

            match line {
                Ok(line) => {
                    if line.ends_with(" AFTP/1.0") {
                        req.extend(line.as_bytes().iter());
                    } else if !line.is_empty() {
                        let mut values = line.split_whitespace();
                        let header = values.next().unwrap().trim_end_matches(":");

                        match header {
                            CONTENTLENGTH => {
                                let value = values.next().unwrap();
                                content_length = value.parse::<usize>().unwrap();
                                println!("content-length: {}", content_length);
                                req.extend(line.as_bytes().iter());
                            }
                            _ => {
                                req.extend(line.as_bytes().iter());
                            }
                        }
                    } else if line.is_empty() {
                        req.extend(b"\r\n".iter());
                        break;
                    }
                }
                Err(e) => {
                    // no string in line
                    println!("Error: {}", e);
                    break;
                }
            }
            req.extend(b"\r\n".iter());
        }

        if content_length > 0 {
            let mut buf = vec![0; content_length];
            reader
                .by_ref()
                .read_exact(&mut buf)
                .expect("Could not read stream into buf");
            req.extend(buf.iter().cloned());
        }

        if req.len() > 0 {
            handle_request(&root_folder, &stream, req, &pub_key, n);
        }
    }

    // TODO add timeout to shutdown stream after no request in defined period
    //stream.shutdown(Shutdown::Both).unwrap();
}

fn handle_request(
    root_folder: &str,
    mut stream: &TcpStream,
    req: Vec<u8>,
    pub_key: &auth::PubKey,
    n: u32,
) {
    let request = str::from_utf8(&req).unwrap();

    let mut lines = request.lines();

    if let Some(first_line) = lines.next() {
        let first_line_as_string = String::from(first_line);

        let cmd = first_line_as_string.split_whitespace().next().unwrap();
        let req_trimmed = first_line_as_string.trim_end_matches(" AFTP/1.0");

        match cmd {
            AUTH => {
                auth::handle_auth(stream, &pub_key, n);
            }
            PUB => {
                auth::handle_pub(stream, &req_trimmed, &pub_key, n);
            }
            LIST => {
                list::handle_list(root_folder, stream, &req_trimmed);
            }
            GET => {
                sync::handle_get(stream, &req_trimmed);
            }
            PUT => {
                sync::handle_put(stream, req);
            }
            DELETE => {
                sync::handle_delete(stream, &req_trimmed);
            }
            _ => {
                println!("Unknown request: {}", cmd);
                stream.write(b"400 Bad request").unwrap();
            }
        }
    }
}
