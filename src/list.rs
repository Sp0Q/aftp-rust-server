use std::convert::TryInto;
use std::fs::File;
use std::io::BufReader;
use std::io::Read;
use std::io::Write;
use std::net::TcpStream;
use std::path::Path;
use std::time::SystemTime;

extern crate walkdir;
use walkdir::WalkDir;

pub fn handle_list(root_path: &str, mut stream: &TcpStream, req: &str) {
    let req_trimmed: String = req
        .trim_start_matches("LIST ")
        .chars()
        .filter(|c| !c.is_whitespace())
        .collect();
    println!("listing contents of {}", &req_trimmed);

    let mut response: String = String::new();

    // TODO use paramater as root_dir (see client)
    let root_dir = Path::new(root_path)
        .canonicalize()
        .expect("Could not canonicalize root_dir");
    let rel_dir = Path::new(req_trimmed.trim_start_matches("/"));
    let full_dir = root_dir
        .join(&rel_dir)
        .canonicalize()
        .expect("Could not canonicalize full_dir");

    // prevent path traversal attacks
    if !full_dir.starts_with(&root_dir) {
        response += "404 Not found\r\n";
    } else {
        let mut list_files: String = String::new();

        for e in WalkDir::new(&full_dir).into_iter().filter_map(|e| e.ok()) {
            let metadata = e.metadata().unwrap();

            // Only send files, no folders because empty folders don't have to be sent
            if metadata.is_file() {
                list_files += e
                    .path()
                    .display()
                    .to_string()
                    .as_str()
                    .trim_start_matches(&root_dir.display().to_string())
                    .trim_start_matches("/");

                // META
                list_files += " ";
                list_files += metadata
                    .modified()
                    .expect("could not get last modified date of file")
                    .duration_since(SystemTime::UNIX_EPOCH)
                    .unwrap()
                    .as_secs()
                    .to_string()
                    .as_str();
                list_files += " ";

                // TODO use file content as input
                let file = File::open(e.path()).expect("Could not open file");

                let mut buf_reader = BufReader::new(file);

                // parses into 64bits usize when on 64bits, tries to parse into 32bits usize when on 32bits system
                // -> this can only have 32bits size files (which should be logical on 32bits systems)
                let mut buf = vec![0; metadata.len().try_into().unwrap()];
                buf_reader
                    .read(&mut buf)
                    .expect("Could not read file content");

                let digest = md5::compute(&mut buf);
                list_files += format!("{:x}", digest).as_str();

                list_files += "\r\n";
            }
        }

        response += "200 OK\r\n";
        response += "Content-Length: ";
        response += list_files.as_bytes().len().to_string().as_str();

        if list_files.as_bytes().len() > 0 {
            response += "\r\n\r\n";
            response = format!("{}{}", response, list_files);
        }
    }

    stream.write(response.as_bytes()).unwrap();
}
