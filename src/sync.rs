use std::convert::TryInto;
use std::fs;
use std::fs::File;
use std::io::BufRead;
use std::io::Read;
use std::io::Write;
use std::io::{BufReader, BufWriter};
use std::net::TcpStream;
use std::path::{Path, PathBuf};
use std::str;

const CONTENTLENGTH: &'static str = "Content-Length";
const ETAG: &'static str = "ETag";

pub fn handle_get(mut stream: &TcpStream, req: &str) {
    let req_trimmed: String = req
        .trim_start_matches("GET ")
        .chars()
        .filter(|c| !c.is_whitespace())
        .collect();
    println!("getting contents of {}", &req_trimmed);

    let mut response: String = String::new();

    // TODO use paramater as root_dir (see client)
    let root_dir = Path::new("testfolder")
        .canonicalize()
        .expect("Could not canonicalize root_dir");
    let rel_dir = Path::new(req_trimmed.trim_start_matches("/"));
    let full_dir = root_dir
        .join(&rel_dir)
        .canonicalize()
        .expect("Could not canonicalize full_dir");

    println!("{:?}", &full_dir);

    // prevent path traversal attacks
    if !&full_dir.starts_with(&root_dir) {
        response += "404 Not found\r\n";
    } else {
        response += "200 OK\r\n";
        response += "Content-Length: ";
        let metadata = fs::metadata(&full_dir).unwrap();
        let content_length = metadata.len() as usize;
        response += &content_length.to_string().as_str();
        response += "\r\n\r\n";

        // send headers without content
        stream.write(response.as_bytes()).unwrap();

        // send content in loop with fixed buffersize
        let file = File::open(&full_dir).expect("Could not open file");
        let mut writer = BufWriter::new(stream);
        let mut reader = BufReader::new(file);

        let mut current_sent_bytes: usize = 0;

        loop {
            let current_bytes = reader.fill_buf().unwrap().len();

            if &current_sent_bytes < &content_length {
                assert!(!reader.buffer().is_empty());

                writer.write(reader.buffer()).unwrap();
                current_sent_bytes += current_bytes;

                reader.consume(current_bytes);
            } else {
                break;
            }
        }
    }
}

pub fn handle_put(mut stream: &TcpStream, req: Vec<u8>) {
    let request = str::from_utf8(&req).unwrap();
    let lines = request.lines();

    let mut path: Option<PathBuf> = None;
    let mut content_length: usize = 0;
    let mut etag: Option<String> = None;

    let mut response: String = String::new();

    for line in lines {
        if line.ends_with(" AFTP/1.0") {
            let req_trimmed: String = line
                .trim_start_matches("PUT ")
                .trim_end_matches(" AFTP/1.0")
                .chars()
                .filter(|c| !c.is_whitespace())
                .collect();
            let root_dir = Path::new("testfolder")
                .canonicalize()
                .expect("Could not canonicalize root_dir");
            let rel_dir = Path::new(req_trimmed.trim_start_matches("/"));
            let full_dir = root_dir
                .join(&rel_dir)
                .canonicalize()
                .expect("Could not canonicalize full_dir");

            // prevent path traversal attacks
            if !&full_dir.starts_with(&root_dir) {
                println!("NOPE");
                panic!("Path traversal attack!");
            } else {
                path = Some(full_dir);
            }
        } else if !line.is_empty() {
            let mut values = line.split_whitespace();
            let header = values.next().unwrap().trim_end_matches(":");

            match header {
                ETAG => {
                    let value = values.next().unwrap();
                    let value_as_string = String::from(value);
                    println!("ETag: {}", &value_as_string);
                    etag = Some(value_as_string);
                }
                CONTENTLENGTH => {
                    let value = values.next().unwrap();
                    content_length = value.parse::<usize>().unwrap();
                    println!("content-length: {}", content_length);
                }
                _ => {
                    println!("Unknown header: '{}'", header);
                }
            }
        } else if line.is_empty() {
            break;
        }
    }

    if content_length > 0 {
        if let Some(p) = &path {
            println!("updating contents of {:?}", &p);

            let request_contents: &[u8] = request.as_bytes();
            let file_content: &[u8] =
                &request_contents[request.bytes().len() - content_length..request.bytes().len()];
            println!("with value: '{:?}'", file_content);

            let local_file_read = File::open(&p);
            match local_file_read.ok() {
                Some(file_unwrapped) => {
                    let metadata = &file_unwrapped.metadata().unwrap();
                    let mut buf_reader = BufReader::new(&file_unwrapped);

                    let mut buf = vec![0; metadata.len().try_into().unwrap()];
                    buf_reader.read(&mut buf).expect("Could not read file");

                    let local_digest = md5::compute(&mut buf);

                    if let Some(e) = &etag {
                        if e.as_str() != format!("{:x}", local_digest).as_str() {
                            println!(
                                "{} != {}",
                                e.as_str(),
                                format!("{:x}", local_digest).as_str()
                            );
                            response += "AFTP/1.0 418 Gone\r\n";
                            // TODO
                            // should return function and send response
                            unimplemented!();
                        }
                    } else {
                        println!("no etag given");
                        // TODO
                        unimplemented!();
                    }
                }
                None => {
                    println!("Could not open file");
                    // TODO
                    unimplemented!();
                }
            }

            println!("overwriting file");

            let mut file_write = File::create(&p).expect("could not create file");
            // let file_content: &[u8] = contents.as_bytes();
            file_write
                .write_all(&file_content)
                .expect("could not write to file");

            let cur_digest = md5::compute(&file_content);

            // return success
            response += "AFTP/1.0 200 OK\r\n";
            response += "Content-Length: ";
            response += format!("{:x}", &cur_digest)
                .as_bytes()
                .len()
                .to_string()
                .as_str();
            response += "\r\n\r\n";
            response += format!("{:x}", &cur_digest).as_str();
            println!("return success");
        }
    } else {
        // TODO
        println!("no content");
        unimplemented!();
    }

    stream.write(response.as_bytes()).unwrap();
}

pub fn handle_delete(mut stream: &TcpStream, req: &str) {
    let req_trimmed: String = req
        .trim_start_matches("DELETE ")
        .chars()
        .filter(|c| !c.is_whitespace())
        .collect();
    println!("deleting {}", &req_trimmed);

    let mut response: String = String::new();

    // TODO use paramater as root_dir (see client)
    let root_dir = Path::new("testfolder")
        .canonicalize()
        .expect("Could not canonicalize root_dir");
    let rel_dir = Path::new(req_trimmed.trim_start_matches("/"));
    let full_dir = root_dir
        .join(&rel_dir)
        .canonicalize()
        .expect("Could not canonicalize full_dir");

    println!("{:?}", &full_dir);

    // prevent path traversal attacks
    if !&full_dir.starts_with(&root_dir) {
        response += "404 Not found\r\n";
    } else {
        fs::remove_file(&full_dir).expect("Could not remove file");

        response += "200 OK\r\n";
    }

    stream.write(response.as_bytes()).unwrap();
}
