use std::io::Write;
use std::net::TcpStream;

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct PubKey {
    pub p: u32,
    pub g: u128,
}

pub fn handle_auth(mut stream: &TcpStream, pub_key: &PubKey, n: u32) {
    // send public key and temp key
    let t: u128 = pub_key.g.pow(n) % pub_key.p as u128;
    println!("{}^{} % {} = {}", pub_key.g, n, pub_key.p, t);

    let pub_key_msg = format!("p={}\r\ng={}\r\nt={}", pub_key.p, pub_key.g, t);

    stream.write(pub_key_msg.as_bytes()).unwrap();
}

pub fn handle_pub(mut stream: &TcpStream, req: &str, pub_key: &PubKey, n: u32) {
    let req_trimmed: String = req
        .trim_start_matches("PUB ")
        .chars()
        .filter(|c| !c.is_whitespace())
        .collect();
    let pub_val: u128 = req_trimmed.parse::<u128>().unwrap();
    let shared_key = pub_val.pow(n) % pub_key.p as u128;
    println!("{}^{} % {} = {}", pub_val, n, pub_key.p, shared_key);
    println!("shared key: {}", shared_key);
    stream.write(b"200 OK").unwrap();
}
